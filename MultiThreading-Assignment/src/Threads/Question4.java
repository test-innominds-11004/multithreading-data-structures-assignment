package Threads;
public class Question4  extends Thread{  
		 public void run(){  
		  if(Thread.currentThread().isDaemon()){//checking for daemon thread  
		   System.out.println("Shows Daemon thread");  
		  }  
		  else{  
		  System.out.println("Shows user thread");  
		 }  
		 }  
		 public static void main(String[] args){  
			 Question4 t1=new Question4();//creating thread  
			 Question4 t2=new Question4();  
			  t1.setDaemon(true);//now t1 is daemon thread  
		    
		  t1.start();//starting threads  
		  t2.start();  
		 
		 }  
		}  

