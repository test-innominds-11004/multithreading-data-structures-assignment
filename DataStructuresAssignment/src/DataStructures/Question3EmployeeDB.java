package DataStructures;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Question3EmployeeDB {
List<Question3Employee> employeeDb= new ArrayList<>();
public boolean addQuestion3Employee(Question3Employee e) {
	return  employeeDb.add(e);
}
public boolean deleteQuestion3Employee(int empId) {
	boolean isRemoved=false;
	
	Iterator<Question3Employee> it=employeeDb.iterator();
	while(it.hasNext()) {
		Question3Employee emp=it.next();
		if(emp.getEmpId()==empId) {
			isRemoved=true;
			it.remove();
		}
	}
	return isRemoved;
}
public String showPaySlip(int empId) {
		String paySlip="Invalid employee id";
		for(Question3Employee e:employeeDb) {
			if(e.getEmpId()==empId) {
				paySlip="Payslip for employee id"+empId+ "is"+e.getEmpSalary();
			}
			return paySlip;
		}
		return paySlip;
}
		public Question3Employee[] listAll() {
			Question3Employee[] empArray=new Question3Employee[employeeDb.size()];
			for(int i=0;i<employeeDb.size();i++) {
				empArray[i]=employeeDb.get(i);
				return empArray;
			}
			return empArray;
				
		

}
}
