package DataStructures;

public class Question3Test {
	public static void main(String[] args) {
		  Question3EmployeeDB empDb = new Question3EmployeeDB();
		  
		  Question3Employee emp1=new Question3Employee(1,"Siddhant","ssd@com",'M',66000);
		  Question3Employee emp2=new Question3Employee(2,"Ranjan","ran@com",'M',55000);
		  Question3Employee emp3=new Question3Employee(3,"Snehith","snn@com",'M',88000);
		  Question3Employee emp4=new Question3Employee(4,"Aameer","aam@com",'M',78000);
		  
		  empDb.addQuestion3Employee(emp1);
		  empDb.addQuestion3Employee(emp2);
		  empDb.addQuestion3Employee(emp3);
		  empDb.addQuestion3Employee(emp4);
		  
		  for(Question3Employee emp : empDb.listAll())
		   System.out.println(emp.GetEmployeeDetails());
		  System.out.println();
		  empDb.deleteQuestion3Employee(2);
		  
		  for(Question3Employee emp : empDb.listAll())
		   System.out.println(emp.GetEmployeeDetails());
		  
		  System.out.println();
		  System.out.println(empDb.showPaySlip(3));
		  
	}

}
